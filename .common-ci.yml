default:
  image: docker:stable
  services:
    - docker:stable-dind

variables:
  # this is used because CI_PROJECT_NAME is not the same
  # as the image name used for this project
  IMAGE_NAME: "container-toolkit"
  DOCKERHUB_REGISTRY: "registry-1.docker.io"

# Define the following dummy targets for specifying make targets through the TARGET variable
.target-ubuntu16:
  variables:
    TARGET: ubuntu16.04

.target-ubuntu18:
  variables:
    TARGET: ubuntu18.04

.target-ubi8:
  variables:
    TARGET: ubi8

# The .build step forms the base of the image builds with each variant
# defined as a build-${VARIANT} step.
.build:
  stage: build
  variables:
    REGISTRY: "${CI_REGISTRY_IMAGE}"
    VERSION: "${CI_COMMIT_SHA}"
  before_script:
    - apk add make
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
  script:
    - make build-${TARGET}
    - make push-${TARGET}

build-ubuntu16:
  extends:
    - .build
    - .target-ubuntu16

build-ubuntu18:
  extends:
    - .build
    - .target-ubuntu18

build-ubi8:
  extends:
    - .build
    - .target-ubi8

# .deploy forms the base of the deployment jobs which push images to the CI registry.
# This is extended with the version to be deployed (e.g. the SHA or TAG) and the
# target os.
.deploy:
  stage: deploy
  before_script:
    # In the case where we are deploying a different version to the CI_COMMIT_SHA, we
    # need to tag the image.
    # Note: a leading 'v' is stripped from the version if present
    - 'echo Version: ${VERSION#v} ; [[ -n "${VERSION#v}" ]] || exit 1'
    - apk add --no-cache make bash
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
    - docker pull "${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_SHA}-${TARGET}"
  script:
    # Log in to the "output" registry, tag the image and push the image
    - docker login -u "${OUT_REGISTRY_USER}" -p "${OUT_REGISTRY_TOKEN}" "${OUT_REGISTRY}"
    - docker tag "${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_SHA}-${TARGET}" "${OUT_IMAGE}:${VERSION#v}-${TARGET}"
    - make IMAGE=${OUT_IMAGE} VERSION=${VERSION#v} push-${TARGET}

# .deploy:sha sets the internal deployment version to the git SHA and deploys to the CI
# container registry
.deploy:sha-ci:
  extends: .deploy
  variables:
    VERSION: "${CI_COMMIT_SHA}"
    OUT_REGISTRY_USER: ${CI_REGISTRY_USER}
    OUT_REGISTRY_TOKEN: ${CI_REGISTRY_PASSWORD}
    OUT_REGISTRY: ${CI_REGISTRY}
    OUT_IMAGE: ${CI_REGISTRY_IMAGE}/${IMAGE_NAME}

deploy:sha-ci-ubuntu16:
  extends:
    - .deploy:sha-ci
    - .target-ubuntu16
  dependencies:
    - build-ubuntu16

deploy:sha-ci-ubuntu18:
  extends:
    - .deploy:sha-ci
    - .target-ubuntu18
  dependencies:
    - build-ubuntu18

deploy:sha-ci-ubi8:
  extends:
    - .deploy:sha-ci
    - .target-ubi8
  dependencies:
    - build-ubi8

# A release job is a deployment job that has a different output version / image
.release:
  extends: .deploy
  stage: release
  variables:
    # We use the CI_COMMIT_REF_SLUG as the release version as this covers tags and non-tags (for staging releases)
    VERSION: "${CI_COMMIT_REF_SLUG}"

# Define a staging release step that pushes an image to an internal "stagin" repository
# This is triggered for all pipelines (i.e. not only tags) to test the pipeline steps
# outside of the release process.
.release:staging:
  extends: .release
  variables:
    OUT_REGISTRY_USER: ${CI_REGISTRY_USER}
    OUT_REGISTRY_TOKEN: ${CI_REGISTRY_PASSWORD}
    OUT_REGISTRY: ${CI_REGISTRY}
    OUT_IMAGE: ${CI_REGISTRY_IMAGE}/staging/${IMAGE_NAME}

.release:external:
  extends: .release
  only:
    - tags

release:staging-ubuntu16:
  extends:
    - .release:staging
    - .target-ubuntu16
  dependencies:
    - deploy:sha-ci-ubuntu16

release:staging-ubuntu18:
  extends:
    - .release:staging
    - .target-ubuntu18
  dependencies:
    - deploy:sha-ci-ubuntu16

release:staging-ubi8:
  extends:
    - .release:staging
    - .target-ubi8
  dependencies:
    - deploy:sha-ci-ubi8
